package com.example.mlandroidapp

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.mlandroidapp.helper.ImageHelperActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickImageActivity(view : View){
        var intent : Intent = Intent(this, ImageHelperActivity::class.java) //In Kotlin, you use the ::class.java syntax to obtain the Class object associated with a class, similar to how you use .class in Java.
        startActivity(intent) //To tell the system that, We are starting Iamge Activity.
    }

}