package com.example.mlandroidapp.helper

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.mlandroidapp.R
import java.io.IOException

class ImageHelperActivity : AppCompatActivity() {

    private val REQUEST_PICK_IMAGE = 1000;
    private lateinit var inputImageview : ImageView
    private lateinit var outputTextview : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_helper)

        inputImageview = findViewById(R.id.imageViewInput)
        outputTextview = findViewById(R.id.textViewOutput)

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){ //To check permission only above Marshmallow version android
            if(checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){ //To check whether permission is already granted or not.
                requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),0)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<out String>,grantResults: IntArray) { //To check whether permission is already granted or not.
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(ImageHelperActivity::class.java.simpleName,"Grant Result for " + permissions[0] + " is " +grantResults[0])
    }

    fun onPickImage(view : View){
             var intent : Intent = Intent(Intent.ACTION_GET_CONTENT) //Which allow the access content from another apps. This should be decide by User at the Runtime which app he can select.
                intent.setType("iamge/*") //Now we are interested here type as image, so we will set the type as image.
                startActivityForResult(intent,REQUEST_PICK_IMAGE)
    }

    fun onPickStartCamera(view : View){

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == RESULT_OK){
            if(resultCode == REQUEST_PICK_IMAGE){
                var uri : Uri? = data?.getData()
                val bitmap: Bitmap? = loadFromuri(uri)
                inputImageview.setImageBitmap(bitmap)
            }
        }
    }

    private fun loadFromuri(uri: Uri?) : Bitmap? {
        if (uri == null) {
            // Handle the case when the Uri is null (e.g., return null or provide a default image)
            return null
        }
        var bitmap : Bitmap? = null
        try{
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.P){
                val source: ImageDecoder.Source = ImageDecoder.createSource(contentResolver, uri) // New Way
                bitmap = ImageDecoder.decodeBitmap(source)
            }else{
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            }
        }
        catch (e : IOException){
            e.printStackTrace()
        }
        return bitmap
    }

}